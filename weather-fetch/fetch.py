#!/usr/bin/python3

import json, requests, sys

class ReportErr(Exception):
    pass

class Report:
    def __init__(self, content: dict):
        if 'current_condition' not in content:
            raise ReportErr('Unable to get current_conditions from json')

        # ref to inner data
        target = content['current_condition'][0]
        # time2fill fields
        self.temp_c = int(target['temp_C'])
        self.hum = int(target['humidity'])

        if len(target['weatherDesc']) < 1:
            self.desc = ''
        else:
            # fuck me this is stupid with all th eindexing
            self.desc = target['weatherDesc'][0]['value']
    @property
    def __dict__(self):
        return {
            'hum': self.hum,
            'temp_c': self.temp_c,
            'desc': self.desc
        }

    def __str__(self):
        '''
        Basically just a pretty print that we use to show on the bar
        '''
        return f"It's {self.temp_c}° @ {self.hum}% humidity Status: {self.desc}"


if __name__ == '__main__':

    try:
        response = requests.get('https://wttr.in/?format=j1')
        # Due to polybar being weird everything goes to stdout
        if response.status_code == 200:
            try:
                report = Report(response.json())
                print(report)
            except ReportErr as re:
                print(re, file=sys.stderr)
            except Exception as e:
                print('unable to decode response payload', file=sys.stderr)
                print('Error caught', e, file=sys.stderr)
        else:
            print('Unable to fetch data from wttr.in', file=sys.stderr)
    except:
        print('Unable to hit wttr API')
