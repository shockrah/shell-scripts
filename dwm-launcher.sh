#!/bin/sh
# This script is used to mimic an xinitrc to some degree

set -x
eval `ssh-agent`

#compton --backend glx --xrender-sync --xrender-sync-fence -b
compton --xrender-sync --xrender-sync-fence -b
bright 50 	# custom brightness script

feh --randomize --bg-fill ~/Pictures/Papes/* &

statline &
exec dwm
