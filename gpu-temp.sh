#!/bin/sh

c_gputemp=`nvidia-smi -q | grep 'GPU Current Temp' | awk '{print $(NF-1)}'`
echo GPU Temp: $c_gputemp C
