#!/bin/sh


size=64
alnum() {
	cat /dev/urandom | tr -dc [:alnum:] | fold -w $size | head -1
}

alnum_spec() {
	cat /dev/urandom | tr -dc [:alnum:]'!@#$%^&*()-_=+' | fold -w $size | head -1
}

_help() {
	echo 'Flags:
	a - alphanumeric set
	s - alphanumeric + special characters'
}

case $1 in
	a) alnum;;
	s) alnum_spec;;
	*) _help;;
esac

