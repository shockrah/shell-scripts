#!/bin/sh

# Kill off previous version of polybar
kill $(pgrep polybar)

if type 'xrandr'; then
	for m in $(xrandr --query | grep ' connected' | cut  -d ' ' -f1);do
		MONITOR=$m polybar --reload cute &
	done
else
	polybar --reload cute &
fi
