# Set up the prompt

autoload -Uz promptinit
#promptinit
#prompt adam1
PROMPT="%K{grey}%F{white}%n%f%k %F{grey} %~%} %F{white} %# %f"

setopt histignorealldups sharehistory

# Remove horrible beep in tty
unsetopt BEEP
# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# 256 color support
[[ "$TERM" == "xterm" ]] && export TERM=xterm-256color

# full file path - someimtes you need a dank copy paste
fpath() {
	echo "$(pwd)/$(ls $1)"
}

# grep colors
alias grep='grep --color'

# arrows
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# ls aliases 
alias ls='ls --color'
alias la='ls -A'
alias ll='ls -alLF'
alias l='ls -CF'

# brightness
alias bup="xbacklight -inc 5;xbacklight"
alias bdown="xbacklight -dec 5;xbacklight"
alias bstat='xbacklight'


# program aliases
alias v='vim'
alias r='ranger'
alias p3='python3'
alias clip='xclip -selection c'
alias xclip='xclip -selection c'


# git stuff
alias ssign='ssh-add ~/.ssh/id_gitlab'
alias gitcheat='mupdf ~/Downloads/gitSheet/git-cheatsheet-EN-dark.pdf &'
alias push='git push'
alias pull='git pull'
alias gst='git status'
alias gd="git diff $1"
alias gr="git remote -v"

# logins to vm's 
alias mininet='ssh mininet@localhost -Y -p 2223'
alias debsand='ssh meme@localhost -Y -p 6969'

# /comfy/ aliases
alias c='clear'
alias cls='clear;ls'
alias e='exit'
alias p='pwd'
alias b='cd ..'
alias rm="rm -i"
alias tc='clear;pwd;l'
alias m='micro'

# rust shiz 
export PATH="$HOME/.cargo/bin:$PATH"

# java trash 
alias java="$HOME/Downloads/jdk-11.0.2/bin/java"
alias javac="$HOME/Downloads/jdk-11.0.2/bin/javac"
export PATH_TO_FX="$HOME/Downloads/javafx-sdk-11.0.2/lib"
