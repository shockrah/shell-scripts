" Pathogen stuff from here
execute pathogen#infect()
syntax on
filetype plugin indent on

set background=dark
set autoindent
set shiftwidth=0
set tabstop=4
set relativenumber
set number
set numberwidth=5
set hlsearch


" f6=copy f7=paste
map <F6> :w !xclip -f -sel clip<CR>
map <F7> ::-1r !xclip -o -sel clip<CR>

" colors 
syntax on
"colorscheme molokai

" vim default for this is dumb
set splitright 
set splitbelow
" Ebin copy pasterino
command Copy execute "w !xclip"
command Paste execute "r !xclcip -o"

" moving around windows
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Writing stuff
let g:limelight_conceal_ctermcfg = 240
autocmd Filetype markdown :Goyo
"autocmd Filetype markdown :Limelight
autocmd Filetype markdown :set spell
autocmd Filetype markdown :set linebreak

" Pandoc compiling 
autocmd Filetype md setlocal makeprg=pandoc\ % -o %:r.md
" Tex compiling
autocmd Filetype tex setlocal makeprg=pdflatex\ %
autocmd Filetype md inoremap <F5> :!echo<space>pandoc<space>-fmarkdown-implicit_figures<space>-f<space>markdown<space>-t<space>pdf<space>%<enter>
autocmd Filetype tex inoremap ,mp :!pdflatex %
