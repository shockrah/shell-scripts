#!/bin/sh

set -e 

# Conditionally start the notification daemon

if pgrep noti > /dev/null ; then
	echo "Notification daemon already started"
else
	echo "Starting notification-daemon"
	/usr/lib/notification-daemon/notification-daemon &
fi
