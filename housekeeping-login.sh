#!/bin/bash

# This I use for setting up 

# Because I always forget to do this
eval `ssh-agent`

# Orient the monitors correctly
# xrandr --output DP-2 --left-of HDMI-0 --auto

# Random background
feh --bg-fill --randomize $HOME/Pictures/Papes/*

# Save my retinas
bright 50

# Compositor for transparency in alacritty
compton -b && echo Compton started || echo Compton probs already running
