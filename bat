#!/bin/bash

# Shows current battery level  from BAT0
# also a script to show off some tricks(hopefully)

raw=`echo -n ' / ' | cat /sys/class/power_supply/BAT0/energy_now - /sys/class/power_supply/BAT0/energy_full | tr -d '\n'`

digits=$(echo $raw | bc -l)
digits=${digits##.}
echo Battery %${digits:0:2}.${digits:3:2}
