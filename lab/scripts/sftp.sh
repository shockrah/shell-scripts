#!/bin/sh

set -x
set -e

keyfile=./ssh/keys/files/files

sftp -i $keyfile \
	-o UserKnownHostsFile=ssh/hosts \
	files@192.168.1.23


