#!/bin/sh

set -x
set -e

keyfile=ssh/keys/local-net/motheradmin

ssh -i $keyfile \
	-o UserKnownHostsFile=ssh/hosts \
	-D 9999 \
	motheradmin@192.168.1.100
