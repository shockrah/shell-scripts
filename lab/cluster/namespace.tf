# This namespace is used for things that we need/want to store somewhere secure
resource kubernetes_namespace vault {
  metadata {
    name = "vault"
  }
}

# This namespace is for more generic things like a simple nginx page or some 
# documentation, etc.
resource kubernetes_namespace web {
  metadata {
    name = "web-services"
  }
}


