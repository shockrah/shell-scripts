# Creating the volume for the vault namespace that we can use
locals {
  vault = {
    volume = {
      modes = [ "ReadWriteOnce" ]
      name = "vault-vol"
      size = "25Gi"
    }
  }
}


resource kubernetes_persistent_volume vault {
  metadata {
    name = local.vault.volume.name
  }
  spec {
    capacity = {
      storage = local.vault.volume.size
    }
    access_modes = local.vault.volume.modes
    persistent_volume_source {
      gce_persistent_disk {
        pd_name = "${local.filebrowser.name}-vol-pd"
      }
    }
  }
}

