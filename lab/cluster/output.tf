output hello {
  value = "${var.cluster_dns}:${kubernetes_service.nginx_plain.spec.0.port.0.port}"
}

output filebrowser {
  value = "${var.cluster_dns}:${kubernetes_service.filebrowser.spec.0.port.0.port}"
}
