resource kubernetes_pod nginx_plain {
  metadata {
    name = "plain-service"
    namespace = kubernetes_namespace.vault.metadata.0.name
    labels = {
      app = "plain-app"
    }
  }
  spec {
    container {
      image = "nginx"
      name  = "plain-hello"
      env {
        name = "arbitrary"
        value = "rando value"
      }
      port {
        container_port = 80
      }
    }
  }
}

resource kubernetes_service nginx_plain {
  metadata {
    name = "plain-service"
    namespace = kubernetes_namespace.vault.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_pod.nginx_plain.metadata.0.labels.app
    }
    port {
      port  = 8080
      target_port = 80
    }
    type = "LoadBalancer"
  }
}
