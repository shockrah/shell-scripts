locals {
  filebrowser = {
    # Name that is basically used everywhere
    name = "filebrowser"
    # For the claim itself
    vol = {
      size = "15Gi"
      mode = "ReadWriteOnce"
    }
  }
}

resource kubernetes_pod filebrowser {
  metadata {
    name = "filebrowser"
    namespace = kubernetes_namespace.vault.metadata.0.name
    labels = {
      app = "filebrowser"
    }
  }
  spec {
    container {
      image = "filebrowser/filebrowser"
      name  = "filebrowser"
      env {
        name  = "TZ"
        value = "PST"
      }
      port {
        container_port = 80
      }
    }
  }
}

resource kubernetes_service filebrowser {
  metadata {
    name = "filebrowser"
    namespace = kubernetes_namespace.vault.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_pod.filebrowser.metadata.0.labels.app
    }
    port {
      port  = 8000
      target_port = 80
    }
    type = "LoadBalancer"
  }
}

resource kubernetes_persistent_volume_claim filebrowser {
  metadata {
    name = "${local.filebrowser.name}-pvc"
  }
  spec {
    access_modes = [ local.filebrowser.vol.mode ]
    resources {
      requests = {
        storage = local.filebrowser.vol.size
      }
    }
    volume_name = "${local.filebrowser.name}-vol"
  }
}

