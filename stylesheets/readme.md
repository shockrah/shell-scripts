# /stylesheets/

This directory contains some stylesheets that I personally like to use on various
websites. Most of these are just to darken sites that I frequent since I try
to not stare at super bright lights all the time.

